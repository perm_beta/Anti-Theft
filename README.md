# Anti Theft

**Anti Theft** is an Android application that notifies a user of the location
of his phone by sending a special SMS to the phone.

## How it works
It uses location and SMS services.
Add `<something secret>` at the beginning of the SMS.
You then will recieve a text message containing the __location__ of 
your phone.
