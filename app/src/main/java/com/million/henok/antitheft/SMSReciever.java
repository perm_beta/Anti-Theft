package com.million.henok.antitheft;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;

import static android.content.Context.LOCATION_SERVICE;
import static android.support.v4.app.ActivityCompat.requestPermissions;

/**
 * Created by User on 4/11/2018.
 */
public class SMSReciever extends BroadcastReceiver {

    void updateLocation(Double longitude, Double latitude) {}
    Double longitude;
    Double latitude;
    LocationManager locationManager;
    LocationListener locationListener;
    Context mContext;


    @Override
    public void onReceive(final Context context, Intent intent) {
        mContext = context;
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
//                Toast.makeText(context, "Location changed: " + location.getLongitude()
//                        + ", "
//                        + location.getLatitude(), Toast.LENGTH_LONG).show();

                longitude = location.getLongitude();
                latitude = location.getLatitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
//                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
//                context.startActivity(intent);
            }
        };

        Bundle pdusBundle = intent.getExtras();
        Object[] pdus = (Object[]) pdusBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte []) pdus[0]);
        Toast.makeText(context, "MESSAGE: " + messages.getMessageBody(), Toast.LENGTH_LONG).show();
        Log.d("Message", "receiving SMS...");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED
                    ) {
//                requestPermissions(new String[] {
//                        Manifest.permission.ACCESS_FINE_LOCATION,
//                        Manifest.permission.ACCESS_COARSE_LOCATION,
//                        Manifest.permission.INTERNET,
//                        Manifest.permission.RECEIVE_SMS,
//                        Manifest.permission.READ_SMS
//
//                }, 1);
                context.startActivity(new Intent(context, MainActivity.class));
            } else {
                Toast.makeText(mContext, "returning...", Toast.LENGTH_SHORT).show();
                requestPermission();
            }
        }

        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        Toast.makeText(mContext, "sending message to " + messages.getOriginatingAddress(), Toast.LENGTH_SHORT).show();
        if (messages.getMessageBody().startsWith("<something secret>")) {
            String receiver = messages.getOriginatingAddress();

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0,
                    locationListener);

            String message = "Longitude: " + location.getLongitude() +
                    " \n Latitude:" + location.getLatitude();

            SmsManager.getDefault().sendTextMessage(receiver, null, message, null, null);
        }
    }
    protected void requestPermission() {
        Log.d("REQUESTING PERMISSION", "HMM....");
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0,
                locationListener);
    }
}
